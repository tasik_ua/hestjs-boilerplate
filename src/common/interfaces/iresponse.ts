import { ResponseStatuses } from '../responseStatuses';

export interface IResponse {
  status: ResponseStatuses.Error | ResponseStatuses.OK;
  data: any;
}
