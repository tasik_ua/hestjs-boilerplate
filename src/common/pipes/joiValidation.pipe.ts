import * as Joi from 'joi';
import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private readonly schema) {}

  transform(rawData: any, metadata: ArgumentMetadata) {
    const options = {
      abortEarly: true,
      convert: true,
      allowUnknown: false,
      stripUnknown: false,
    };
    const { error, value } = Joi.validate(rawData, this.schema, options);
    if (error) {
      const errorMessage = `Validation failed! Error: ${error.details[0].message}`;
      throw new BadRequestException(errorMessage);
    }
    return value;
  }
}
