import { IResponse } from './interfaces/iresponse';
import {ResponseStatuses} from './responseStatuses';

export default ( data?: any): IResponse => {
  return {
    status: ResponseStatuses.OK,
    data,
  };
};
