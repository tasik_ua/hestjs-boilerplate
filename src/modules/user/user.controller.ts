import {Controller, Get, Patch, Delete, Param, Res, Post, UsePipes, Body} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import { ConfigService } from '../config/config.service';
import { JoiValidationPipe } from '../../common/pipes/joiValidation.pipe';
import responseFactory from '../../common/response.factory';
import { newUser } from './schemas/createUser.schema';
import { NotFoundException } from '@nestjs/common';

@Controller('users')
export class UserController {

  constructor(
    private readonly userService: UserService,
    private readonly configService: ConfigService,
  ) {}

  @Get()
  public async findAll(@Res() res) {
    const users = await this.userService.findAll();
    res.json(responseFactory(users));
  }

  @Get(':id')
  public async find(@Param('id') id, @Res() res) {
    const user = await this.userService.find(id);

    if (!user) {
      throw new NotFoundException();
    }

    res.json(responseFactory(user));
  }

  @Post()
  @UsePipes(new JoiValidationPipe(newUser))
  public create(@Body() data) {
    return {k: 'k'};
  }
}
