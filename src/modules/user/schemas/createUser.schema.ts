import * as Joi from 'joi';

export const newUser = Joi.object({
  username: Joi.string().min(5).max(100).required(),
});
