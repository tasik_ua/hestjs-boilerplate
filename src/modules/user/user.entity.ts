import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn} from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    nullable: true,
  })
  username: string;

  @Column({
    length: 100,
  })
  email: string;

  @Column({
    length: 20,
    nullable: true,
    select: false,
  })
  password: string;

  @Column({
    default: false,
  })
  isActive: boolean;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;
}
