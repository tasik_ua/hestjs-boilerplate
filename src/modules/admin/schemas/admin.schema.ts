import { object, string, ObjectSchema } from 'joi';

export const createAdmin: ObjectSchema = object({
  username: string().min(3).max(100).required(),
  email: string().min(5).max(100).email().required(),
  password: string().min(10).max(100).required(),
  description: string().min(5).max(100),
});
