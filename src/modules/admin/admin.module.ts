import { Module, MiddlewareConsumer } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { AdminController } from './admin.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JoinParamsMiddleware } from '../../common/middlewares/joinParams.middleware';
import { Admin } from './admin.entity';
import { AdminService } from './admin.service';

@Module({
  imports: [TypeOrmModule.forFeature([Admin]), ConfigModule],
  providers: [AdminService],
  controllers: [AdminController],
  exports: [AdminService],
})
export class AdminModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JoinParamsMiddleware)
      .forRoutes(AdminController);
  }

}
