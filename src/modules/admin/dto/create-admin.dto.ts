export class CreateAdminDto {
  readonly username: string;
  readonly email: string;
  readonly password: string;
  readonly description: string;
}
