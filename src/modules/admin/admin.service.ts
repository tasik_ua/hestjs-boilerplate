import {ConflictException, Injectable} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Admin } from './admin.entity';
import { ConfigService } from '../config/config.service';
import {count} from "rxjs/operators";

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(Admin)
    private readonly adminRepository: Repository<Admin>,
    private readonly configService: ConfigService,
  ){}

  public async create(adminInfo): Promise<void> {
    const isExist = await this.adminRepository.createQueryBuilder('admins')
      .where('admins.email = :email', { email: adminInfo.email })
      .orWhere('admins.username = :username', { username: adminInfo.username })
      .getCount();
    if (isExist !== 0) {
      throw new ConflictException('User with this username or email already registered!');
    }
    const admin: Admin = new Admin();
    admin.username = adminInfo.username;
    admin.email = adminInfo.email;
    admin.description = adminInfo.description;
    admin.password = await this.encryptPassword(adminInfo.password);
    await this.adminRepository.save(admin);
  }

  public async encryptPassword(password: string): Promise<string> {
    return await bcrypt.hash(password, this.configService.get('PASSWORD_SALT_COUNT'));
  }

  public async comparePassword(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }

  public async checkAuth(login): Promise<Admin> {
    const admin: Admin = (await this.adminRepository.find({ email: login.email }))[0];
    if (!admin) {
      return null;
    }
    const isMatch: boolean = await this.comparePassword(login.password, admin.password);
    if (!isMatch) {
      return null;
    }
    return admin;
  }

  public async findAdminById(id: number): Promise<Admin> {
    return await this.adminRepository.findOne({ id, isBlocked: false});
  }
}
