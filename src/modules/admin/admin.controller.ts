import {Controller, Get, Patch, Delete, Param, Res, Post, UsePipes, Body, HttpStatus} from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { JoiValidationPipe } from '../../common/pipes/joiValidation.pipe';
import { createAdmin } from './schemas/admin.schema';
import responseFactory from '../../common/response.factory';
import { AdminService } from './admin.service';
import { CreateAdminDto } from './dto/create-admin.dto';

@Controller('admins')
export class AdminController {

  constructor(
    private readonly configService: ConfigService,
    private readonly adminService: AdminService,
  ) {}

  @Get()
  public async findAll(@Res() res) {
    res.json(responseFactory({k: 'k'}));
  }

  @Get(':id')
  public async find(@Param('id') id, @Res() res) {

  }

  @Post()
  @UsePipes(new JoiValidationPipe(createAdmin))
  public async create(@Body() createAdminDto: CreateAdminDto, @Res() res) {
    await this.adminService.create(createAdminDto);
    return res.status(HttpStatus.CREATED).json(responseFactory({}));
  }
}
