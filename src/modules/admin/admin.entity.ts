import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, BeforeInsert, BeforeUpdate, Unique } from 'typeorm';

@Entity('admins')
export class Admin {
  private static DEFAULT_SALT_ROUNDS = 10;

  @PrimaryGeneratedColumn({
    type: 'bigint',
  })
  public id: number;

  @Column({
    length: 100,
    nullable: true,
  })
  public username: string;

  @Column({
    length: 100,
  })
  public email: string;

  @Column({
    length: 100,
    nullable: true,
  })
  public password: string;

  @Column({
    length: 100,
    nullable: true,
  })
  public description: string;

  @Column({
    default: false,
  })
  public isBlocked: boolean;

  @CreateDateColumn()
  public createdAt: string;

  @UpdateDateColumn()
  public updatedAt: string;


}
