import {CanActivate, ExecutionContext} from "@nestjs/common";

export class RoleGuard implements CanActivate{
  public role: string;

  constructor(role: string) {
    this.role = role;
  }

  public canActivate(context: ExecutionContext): boolean {
    console.log('permission - ', this.role);

    console.log(context.switchToHttp().getRequest().user);
    return true;
  }
}
