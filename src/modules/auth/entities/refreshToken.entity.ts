import { Entity } from 'typeorm';
import { Token } from './token.entity';

@Entity('refreshTokens')
export class RefreshToken extends Token {}
