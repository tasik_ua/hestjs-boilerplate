import {Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

export abstract class Token {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    type: 'bigint',
  })
  public adminId: number;

  @Column({
    type: 'text',
  })
  public token: string;

  @Column({
    type: 'int',
  })
  public expireTime: number;

  @Column({
    default: false,
  })
  public isBlocked: boolean;

  @CreateDateColumn()
  public createdAt: string;

  @UpdateDateColumn()
  public updatedAt: string;
}
