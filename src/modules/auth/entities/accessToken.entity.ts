import { Entity } from 'typeorm';
import { Token } from './token.entity';

@Entity('accessTokens')
export class AccessToken extends Token {}
