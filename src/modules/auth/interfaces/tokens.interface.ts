export interface Tokens {
  accessToken: string;
  accessExpiresIn: number;
  refreshToken?: string;
  refreshExpiresIn?: number;
}
