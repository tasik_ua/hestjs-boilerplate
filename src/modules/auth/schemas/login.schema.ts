import { object, string, ObjectSchema } from 'joi';

export const login: ObjectSchema = object({
  email: string().min(5).max(100).email().required(),
  password: string().min(10).max(100).required(),
});
