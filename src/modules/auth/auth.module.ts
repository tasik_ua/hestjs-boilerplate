import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './passport-stategies/jwt.strategy';
import { AccessToken } from './entities/accessToken.entity';
import { RefreshToken } from './entities/refreshToken.entity';
import { JoinParamsMiddleware } from '../../common/middlewares/joinParams.middleware';
import { AdminModule } from '../admin/admin.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([AccessToken, RefreshToken]),
    ConfigModule,
    AdminModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: process.env.JWT_SECRET,
      signOptions: {
        algorithm: 'HS512',
        expiresIn: 1000000,
        // expiresIn: process.env.JWT_TIME_SHORT,
      },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JoinParamsMiddleware)
      .forRoutes(AuthController);
  }
}
