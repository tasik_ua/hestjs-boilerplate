import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { AccessToken } from './entities/accessToken.entity';
import { RefreshToken } from './entities/refreshToken.entity';
import { Tokens } from './interfaces/tokens.interface';
import { Admin } from '../admin/admin.entity';
import { AdminService } from '../admin/admin.service';
import {log} from "util";

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AccessToken)
    private readonly accessTokenRepository: Repository<AccessToken>,
    @InjectRepository(RefreshToken)
    private readonly refreshTokenRepository: Repository<RefreshToken>,
    private readonly jwtService: JwtService,
    private readonly adminService: AdminService,
  ) {}

  async createAccessToken(payload: JwtPayload): Promise<string> {
    const expireTime: number = parseInt(process.env.JWT_TIME_SHORT, 10);
    const token = this.jwtService.sign(payload, {
      expiresIn: expireTime,
    });
    const accessToken: AccessToken = new AccessToken();
    accessToken.adminId = payload.id;
    accessToken.token = token;
    accessToken.expireTime = expireTime;
    await this.accessTokenRepository.save(accessToken);
    return token;
  }

  async createRefreshToken(payload: JwtPayload): Promise<string> {
    const expireTime = parseInt(process.env.JWT_TIME_LONG, 10);
    const token = this.jwtService.sign(payload, {
      expiresIn: expireTime,
    });
    const refreshToken: RefreshToken = new RefreshToken();
    refreshToken.adminId = payload.id;
    refreshToken.token = token;
    refreshToken.expireTime = expireTime;
    await this.refreshTokenRepository.save(refreshToken);
    return token;
  }

  async createTokens(payload: JwtPayload): Promise<Tokens> {
    const accessToken: string = await this.createAccessToken(payload);
    const refreshToken: string = await this.createRefreshToken(payload);
    const tokens: Tokens = {
      accessToken,
      accessExpiresIn: (process.env.JWT_TIME_SHORT as unknown as number),
      refreshToken,
      refreshExpiresIn: (process.env.JWT_TIME_LONG as unknown as number),
    };
    return tokens;
  }

  async validateAdmin(payload: JwtPayload): Promise<JwtPayload> {
    const admin = await this.adminService.findAdminById(payload.id);
    if (!admin) {
      return null;
    }
    return {id: admin.id};
  }
}
