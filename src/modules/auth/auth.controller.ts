import {Body, Controller, Get, Post, UseGuards, UsePipes, Res, UnauthorizedException, Req} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { login } from './schemas/login.schema';
import { JoiValidationPipe } from '../../common/pipes/joiValidation.pipe';
import { LoginDto } from './dto/login.dto';
import { AdminService } from '../admin/admin.service';
import responseFactory from '../../common/response.factory';
import { Tokens } from './interfaces/tokens.interface';
import { Admin } from '../admin/admin.entity';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { User } from '../../common/decorators/user.decorator';
import { RoleGuard } from './guards/role-guard';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly adminService: AdminService,
    private readonly authService: AuthService,
  ) {}

  @Post('login')
  @UsePipes(new JoiValidationPipe(login))
  public async login(@Body() loginDto: LoginDto, @Res() res) {
    const admin: Admin = await this.adminService.checkAuth(loginDto);
    if (!admin) {
      throw new UnauthorizedException('Something wrong! Try another login or password!');
    }
    const tokens: Tokens = await this.authService.createTokens({ id: admin.id });
    return res.json(responseFactory(tokens));
  }

  @Post('logout')
  @UseGuards(new JwtAuthGuard(), new RoleGuard('admin'))
  public async logout() {
    return { you: 'allah' };
  }

  @Get('data')
  @UseGuards(new JwtAuthGuard())
  public findAll(@User() admin) {
    console.log(admin);
  }
}
