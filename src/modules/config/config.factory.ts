import { ConfigServiceInterface } from './interfaces/config.service.interface';
import DevelopmentConfig from './developmentConfig.service';
import ProductionConfig from './productionConfig.service';
import TestConfig from './testConfig.service';

export default (env: string): ConfigServiceInterface => {
  let configService: ConfigServiceInterface;
  switch (env) {
    case 'development':
      configService = new DevelopmentConfig();
      break;
    case 'production':
      configService = new ProductionConfig();
      break;
    case 'test':
      configService = new TestConfig();
      break;
    default:
      throw new Error(`Config does not exist for ${env} environment`);
  }
  return configService;
};
