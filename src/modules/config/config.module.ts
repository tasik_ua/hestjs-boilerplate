import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Config } from './config.entity';
import { ConfigController } from './config.controller';
import { ConfigService } from '../config/config.service';

@Module({
  imports: [TypeOrmModule.forFeature([Config])],
  providers: [
    ConfigService,
  ],
  controllers: [ConfigController],
  exports: [ConfigService],
})
export class ConfigModule {}
