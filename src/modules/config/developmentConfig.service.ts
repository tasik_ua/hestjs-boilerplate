import { ConfigServiceInterface } from './interfaces/config.service.interface';
import commonConfig from './configs/common.config';
import developmentConfig from './configs/development.config';

export default class DevelopmentConfigService implements ConfigServiceInterface {
  readonly config: { [key: string]: string | number };

  constructor() {
    this.config = Object.assign({}, commonConfig, developmentConfig);
  }

  public get(key: string): string | number {
    return this.config[key];
  }
}
