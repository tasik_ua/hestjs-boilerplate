import { Injectable } from '@nestjs/common';

import commonConfig from './configs/common.config';
import testConfig from './configs/test.config';
import { ConfigServiceInterface } from './interfaces/config.service.interface';

@Injectable()
export default class TestConfigService implements ConfigServiceInterface {
  readonly config: { [key: string]: string | number };

  constructor() {
    this.config = Object.assign({}, this.config, commonConfig, testConfig);
  }

  public get(key: string): string | number {
    return this.config[key];
  }
}
