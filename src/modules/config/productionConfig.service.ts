import { Injectable } from '@nestjs/common';
import commonConfig from './configs/common.config';
import productionConfig from './configs/production.config';
import { ConfigServiceInterface } from './interfaces/config.service.interface';

@Injectable()
export default class ProductionConfigService implements ConfigServiceInterface {
  readonly config: { [key: string]: string | number };

  constructor() {
    this.config = Object.assign({}, this.config, commonConfig, productionConfig);
  }

  public get(key: string): string | number {
    return this.config[key];
  }
}
