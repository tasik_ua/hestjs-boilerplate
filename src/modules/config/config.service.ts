import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import configFactory from './config.factory';
import { Config } from './config.entity';
import { ConfigServiceInterface } from './interfaces/config.service.interface';
import { ConfigDBServiceInterface } from './interfaces/configDB.service.interface';

@Injectable()
export class ConfigService  implements ConfigServiceInterface, ConfigDBServiceInterface {
  public config: ConfigServiceInterface;

  constructor(
    @InjectRepository(Config)
    private readonly configRepository: Repository<Config>,
  ) {
    this.config = configFactory(process.env.NODE_ENV);
  }

  public get(key: string): string | number {
    return this.config.get(key);
  }

  public async getFromDB(key): Promise<string | number> {
    const config = await this.configRepository.findOne({ where: { key } });

    if (!config) {
      throw new Error(`Config not found! Config key: ${key}`);
    }

    return config.value;
  }

  public async getAllFromDB(): Promise<Config[]> {
    return this.configRepository.find();
  }
}
