export interface ConfigDBServiceInterface {
  getFromDB(key: string): Promise<string | number>;
}
