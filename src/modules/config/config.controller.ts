import {Controller, Get, Patch, Put, Res} from '@nestjs/common';
import { ConfigService } from './config.service';
import { Config } from './config.entity';
import { ConfigInterface } from './interfaces/config.interface';
import responseFactory from '../../common/response.factory';

@Controller('config')
export class ConfigController {
  constructor(
    private readonly configService: ConfigService,
  ) {}

  @Get()
  async get(@Res() res) {
    const configs = await this.configService.getAllFromDB();
    res.json(responseFactory(configs));
  }

  @Get('one')
  async getOne(@Res() res) {
    const value = await this.configService.getFromDB('pagination');
    res.json(responseFactory({ value }));
  }

  @Put()
  async update() {
    return '';
  }
}
