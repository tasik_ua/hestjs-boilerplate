import { Module } from '@nestjs/common';
import { ConfigModule } from './modules/config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BaseModule } from './modules/base/base.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { AdminModule } from './modules/admin/admin.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.TYPEORM_HOST,
      port: parseInt(process.env.TYPEORM_PORT, 10),
      username: process.env.TYPEORM_USERNAME,
      password: process.env.TYPEORM_PASSWORD,
      database: process.env.TYPEORM_DATABASE,
      synchronize: true,
      logging: (process.env.TYPEORM_LOGGING === 'true') ? true : false,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      cache: {
        type: 'redis',
        options: {
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          prefix: process.env.DB_NAME + '_',
        },
      },
    }),
    AuthModule,
    BaseModule,
    ConfigModule,
    AdminModule,
    UserModule,
  ],
})

export class AppModule {}
