FROM node:10.10-slim

RUN mkdir -p /usr/src/adara-academy/
WORKDIR /usr/src/adara-academy/

COPY package.json yarn.lock /usr/src/adara-academy/
RUN yarn install

ADD . /usr/src/adara-academy/

RUN yarn build
RUN yarn install --production=true

FROM node:10.10-slim

ENV NODE_ENV production
ENV logger:prettyPrint false

WORKDIR /usr/src/adara-academy/

COPY --from=0 /usr/src/adara-academy/build /usr/src/adara-academy/build
COPY --from=0 /usr/src/adara-academy/node_modules /usr/src/adara-academy/node_modules
COPY --from=0 /usr/src/adara-academy/config /usr/src/adara-academy/config
COPY --from=0 /usr/src/adara-academy/entrypoint.sh /usr/src/adara-academy/entrypoint.sh

CMD ./entrypoint.sh src/server.js
